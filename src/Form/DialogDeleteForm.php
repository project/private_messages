<?php

namespace Drupal\private_messages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Dialog entities.
 *
 * @ingroup private_messages
 */
class DialogDeleteForm extends ContentEntityDeleteForm {


}
