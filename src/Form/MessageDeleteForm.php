<?php

namespace Drupal\private_messages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Message entities.
 *
 * @ingroup private_messages
 */
class MessageDeleteForm extends ContentEntityDeleteForm {


}
